package com.example.farizdotid.belajarkotlin.activity

import android.app.ProgressDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.farizdotid.belajarkotlin.R
import com.example.farizdotid.belajarkotlin.server.BaseApiService
import com.example.farizdotid.belajarkotlin.model.InfoUser
import com.example.farizdotid.belajarkotlin.server.UtilsApi
import kotlinx.android.synthetic.main.activity_github.*
import retrofit2.Call
import retrofit2.Response

class GithubActivity : AppCompatActivity() {

    var loading : ProgressDialog? = null

    var mContext : Context? = null
    var mApiService : BaseApiService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_github)

        mContext = this
        mApiService = UtilsApi.getApiService()

        loading = ProgressDialog(this)

        getData()
    }

    private fun getData(){
        loading!!.setMessage("Harap Tunggu...")
        loading!!.setCancelable(false)
        loading!!.show()
        mApiService!!.getInfoUser("farizdotid").enqueue(object : retrofit2.Callback<InfoUser> {
            override fun onResponse(call: Call<InfoUser>, response: Response<InfoUser>) {
                tvNama.text = response.body().name
                Glide.with(mContext).load(response.body().avatarUrl).into(ivAvatar)
                tvCompany.text = response.body().company
                tvBlog.text = response.body().blog
                tvBio.text = response.body().bio

                loading!!.dismiss()
            }

            override fun onFailure(call: Call<InfoUser>, t: Throwable) {
                loading!!.dismiss()
            }
        })
    }
}
