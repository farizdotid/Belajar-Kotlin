package com.example.farizdotid.belajarkotlin.util

import android.content.Context
import android.widget.Toast

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */


object Helper {

    fun showToast(mContext: Context, message: String) {
        Toast.makeText(mContext, "HAIL KOTLIN " + message, Toast.LENGTH_SHORT).show()
    }

}
