package com.example.farizdotid.belajarkotlin.activity

import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.farizdotid.belajarkotlin.R
import com.example.farizdotid.belajarkotlin.util.Helper
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSelesai.setOnClickListener { _ ->
            run {
                if (etNama.text.isEmpty()){
                    etNama.error = "Isi Terlebih Dahulu"
                } else {
                    Helper.showToast(this, etNama.text.toString())
                }
            }
        }
        btnGithubAct!!.setOnClickListener{ _ ->
            run {
                val intent = android.content.Intent(this, GithubActivity::class.java)
                startActivity(intent)
            }
        }
    }
}