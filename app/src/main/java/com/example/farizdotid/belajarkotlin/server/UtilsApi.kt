package com.example.farizdotid.belajarkotlin.server

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */

object UtilsApi {

    val BASE_URL : String = "https://api.github.com/"

    fun getApiService() : BaseApiService? {
        return RetrofitClient.getClient(BASE_URL)!!.create(BaseApiService::class.java)
    }
}