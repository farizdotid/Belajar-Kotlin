package com.example.farizdotid.belajarkotlin.server

import com.example.farizdotid.belajarkotlin.model.InfoUser
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */

interface BaseApiService {

    @GET("users/{username}")
    fun getInfoUser(@Path("username") username : String?) : Call<InfoUser>
}